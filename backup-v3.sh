#!/bin/bash


if [ $UID != 0 ]; then
	echo "Ejecute este script con privilegios de 'root'."
	exit 1
fi



# el script debe recibir argumentos, estos argumentos determinan:
#	usuario
#	directorio destino del backup
#	formato del listado de archivos

user=$1

dest=$2

list=$3

date=$(date +%Y-%d-%m_%H-%M)

# Creamos el directorio con los datos pasados como argumento.

mkdir -p $dest/$user-$date

cp -pr /home/$user $dest/$user-$date


echo "Backup realizado"

ls -R$list $dest/$user-$date 


