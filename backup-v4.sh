#!/bin/bash

log_file=salida.log


# Uso del programa
# ./backup-v4.sh s parametro d parametro
#
# 'd' Argumento que indica el destino
# se acompaña del parametro '/ruta/al/directorio'
#
# 's' Argumento que indica el origen
# se acompaña '/ruta/al/directorio'
#
# EJ.
# ./backup-v4.sh s /var/www-data d /var/backups


# declara un array vacio

declare -a arr

i=0

# Bucle for

for arg in $*; do

	arr[$i]=$arg
	((i++))
done


echo "Inicia backup $(date)" | tee $log_file

echo "" | tee -a $log_file

if [ ${arr[0]} = s ]; then

	echo ${arr[1]}
else

	echo "El primer argumento debe ser 's /ruta/al/directorio'"
	exit 1
fi


if [ ${arr[2]} = d ]; then
	echo ${arr[3]}
else
	echo "El segundo argumento debe ser 'd /ruta/al/directorio'"
	exit 1

fi

exit

rsync -av ${arr[0]} ${arr[1]} 2>&1 | tee -a $log_file 



