i=0

while [ $i -lt 10 ]; do

if [ $i -eq 5 ]; then
	# se pasa a la suiguiente iteracion
	((i++))
	continue	
fi

# si se ejecuto el 'continue' esta parte no se ejecuta
echo $i
((i++))

done

# y se prosigue con el resto del programa
echo "Se ejecuta el comando 'echo' fuera del bucle"
