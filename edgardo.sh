#!/bin/bash
var="backup-$USER-$(date +%Y-%d-%m_%H-%M)"
mkdir -p /tmp/$var
find ./ -type f -size +10M -exec cp -p {} /tmp/$var \;
echo "El backup ha finalizado"
find /tmp/$var -type f -exec ls -lhR {} \;
du -h /tmp/$var
