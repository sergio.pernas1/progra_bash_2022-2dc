#!/bin/bash


# como usar este programa
#
# ./hoststatus.sh example.com page.net anothersite.tv ...
#
# El programa

# Almacenar los argumentos pasados en un array

	<codigo>

# Crear el fichero 'resumen.out' con el siguiente contenido:
# "Test iniciado el <fecha y hora>'
# Se realizaran una serie de comprobaciones sobre los siguientes hosts:
# - <primer argumento>
# - <segundo argumento>
# - <tercer argumento>
# - <... argumento>
#
# Resoluciones de direcciónes de red:"

	<codigo>

# Ejecutar el comando 'host' con cada elemento del array.
# La salida del comando aplicado a cada elemento
# se debe añadir al fichero 'resumen.out'.

	host <elemento>

# Añadir al fichero 'resumen.out':
# "Tiempos de respuesta:"
	
	<codigo>

# ejecutar el comando 'ping' con cada elemento del array y añadir
# la salida al fichero 'resumen.out' con el siguiente formato
# "Nombre de host: <elemento>
# <las ultimas 2 lineas de la salida del comando ping>"

	ping -c 1 -q <elemento>


