#!/bin/bash

func_logger () {

	echo "$(date) - response: $1" >> http_status.log
}

while :;do

	code=$(curl -s -I http://192.168.0.25 | grep ^HTTP | cut -d " " -f 2)

	if [ ${#code} -eq 0 ];then
		func_logger inalcanzable
		sleep 2
		continue
	fi

	if [[ $code -lt 200 ]] || [[ $code -gt 299 ]];then
		func_logger $code
	fi

	sleep 2 
done
