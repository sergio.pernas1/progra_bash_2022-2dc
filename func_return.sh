
# lo que el script recibe como argumento
arg=$1


func_test () {
	# lo que la funcion recibe como argumento
	local proto=$1
	if [ "$proto" -ne "4" ] && [ "$proto" -ne "6" ];then
		return 1
        fi
}


func_help () {

	echo "Debe introducir un valor que sea 4 o 6"

}

# invocamos la funcion con lo que el script recibio como argumento
func_test $arg && echo $arg || func_help

if func_test $arg; then

	echo $arg
else
	func_help

fi
