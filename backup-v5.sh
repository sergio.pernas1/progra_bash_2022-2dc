#!/bin/bash

declare -a arr

i=0


for arg in $*; do

	arr[$i]=$arg
	((i++))
done


# backup.sh s /ruta/origen d /ruta/destino

if [ ${arr[0]} = d ]; then

	echo ${arr[1]}

elif [ ${arr[2]} = s ]; then

	echo ${arr[3]}
else
	echo "modo de uso: $0 s /ruta/origen d /ruta/destino"
fi
