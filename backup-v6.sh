#!/bin/bash

# La variable '$arg' asume la cadena correspondiente
# a cada elemento de la lista del bucle

for arg in $*; do

# Se compara el valor de la variable con algun elemento
# de la sentencia 'case'
case $arg in

	s=*)
		# validamos ruta origen
		# 'trimendo' la cadena para solo dejar
		# la parte correspondiente a la ruta.
		if [ -d ${arg:2} ]; then
			# guardamos la cadena validad
			# en una variable
			param1=${arg:2}
		else
			echo "Ruta invalida."
			exit 1
		fi
		;;
	d=*)
		if [ -d ${arg:2} ]; then
			param2=${arg:2}
		else
			echo "Ruta invalida."
			exit 1
		fi
		;;
	*)	# Inmprimimos este mensaje si no se pasa un argumento valido
		echo "Modo de uso: $0 s=/ruta/origen d=/ruta/destino"
	;;
esac

done

# se orndena la informacion valida y se la pasa al comando 'rsync'

echo "rsync $param1 $param2"
