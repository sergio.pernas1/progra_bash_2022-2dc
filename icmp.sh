#!/bin/bash
 
args=( $@ )
 

func_validate () {

	local input=$1
	local test=$2

	case $test in
		cantidad)
			# validar la opcion '-c' su argumento debe ser un numero positivo 
			# al invertir la condicion (!) si $input no es mayor o igual que 1
			# la funcion devuelve 'return 1'
			if ! [ $input -ge 1 ]; then
				return 1
		        fi
			;;
		protocolo)

			if [ "$input" -ne "4" ] && [ "$input" -ne "6" ];then
				return 1
			fi

	esac
}

func_help () {

	local input=$1

	echo "modo de uso"

	echo -e "./icmp_util.sh -C n -p n\n"
	echo -e "Opciones:\n"
	echo "	-C n      Numero de pings a realizar debe ser un entero positivo."
	echo "	-p n      Protocolo a utilizar, 4 para ipv4 y 6 para ipv6."

exit $input
}

# Obtenemos la lista de indices: 0 1 2 3 ... 
for opcion in "${!args[@]}"; do
 
	# llamamos al indice del array segun el valor actual de la variable $opcion
    case  ${args[$opcion]}  in
 
        -c)
		# llamamos al indice siguiente sumando 1 al valor actual de la variable $opcion
		val="${args[$(($opcion+1))]}"

		# ese valor se pasa como argumento a la funcion.
		# dentro de la funcion:
		#	     |  $input | $test  |
		if func_validate $val   cantidad; then

			# si la funcion devuelve 'return 0'
			counter="-c $val"
	 	else
			func_help $?
		fi		

		;;
	-p)
		val="${args[$(($opcion+1))]}"

		if func_validate $val protocolo; then

			p="-$val"

		else
			func_help $?

		fi
                ;;
    	-h)
		func_help 0
        	;;
 
	esac
done
 
echo "
ping $counter $p ${args[-1]}
"
 
