#!/bin/bash

# variables

i=0
read -p "Nombre de usuario: " user
declare -a user_fields

# si el comando 'grep' no encuentra la cadena en el fichero
# devuelve un exit status distinto de 0.
# con el operador 'or' salimos del programa y establecemos
# un exit statos con error para este script.

grep ^$user /etc/passwd || echo "no existe el usuario"

user_search="$(grep -w ^$user /etc/passwd)"
ifs="$IFS"
IFS=":"

for item in $user_search;do

	user_fields[$i]=$item
	((i++))

done

IFS=$ifs

echo ""
echo -e "Usuario:\t\t${user_fields[0]}\n"
echo -e "Contraseña:\t\t${user_fields[1]}\n"
echo -e "ID usuario:\t\t${user_fields[2]}\n"
echo -e "ID grupo:\t\t${user_fields[3]}\n"
echo -e "Comentarios:\t\t${user_fields[4]}\n"
echo -e "Directorio de usuario:\t${user_fields[5]}\n"
echo -e "Interprete:\t\t${user_fields[6]}\n"

