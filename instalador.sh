#!/bin/bash

# creamos una variable de uso recurrente

installdir="$HOME/.bin"

# crear un directorio llamado '.bin' en el directorio de usuario.

mkdir $installdir

# Crear fichero con el programa para testear red.

echo "#!/bin/bash

ping -c 1 8.8.8.8 > /dev/null 2>&1 && echo \"hay internet\" || echo \"no hay internet\"" > $installdir/nettest

# Permisos de ejecucion para el programa

chmod +x $installdir/nettest


# Agregar la nueva ruta de binarios a la variable de entorno $PATH
echo "export PATH=\$PATH:$installdir" >> $HOME/.bashrc

# Instucciones

echo "Luego de instalar ejecute:

	\$ source ~/.bashrc

Test internet:

	$ nettest"
