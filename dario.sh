#!/bin/bash

var="backup-${USER}_$(date +%Y-%d-%m_%H-%M)"

mkdir -p /tmp/$var

find ./ -type f -size +10M -exec cp -p {} /tmp/$var \;

du -h /tmp/$var
ls -l /tmp/$var

echo "El backup a finalizado de manera correcta"

