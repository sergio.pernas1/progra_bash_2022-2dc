#!/bin/bash

# El script debe buscar dentro del directorio de usuario ficheros
# de mas de 10 MB y copiarlos a un directorio con el nombre 
# 'backup-fecha_hora-minutos', este directorio debe estar en '/tmp'.

var="backup-$(date +%Y-%d-%m_%H-%M)"

mkdir -p /tmp/$var

find ./ -type f -size +10M -exec cp -p {} /tmp/$var \;
