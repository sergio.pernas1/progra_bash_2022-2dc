#!/bin/bash



file=/var/log/apache2/access.log

# se pasa un fichero como argumento del while, este devolvera true siempre
# que exista una nueva linea, en el lugar de la comparacion se invoca
# la instruccion 'read' almacenara las cadenas de cada linea en la variable 'line'
# en cada vuelta, es decir, con cada liena nueva

i=0
declare -A stats=([host,0]="" [cant,0]="")

# Matriz
# 	0		1		2		3		...
# host	<iphost>	<iphost>	<iphost>	<iphost>	...
# cant	n		n		n		n		...


while read -r line; do

	host=$( echo $line | cut -d '-' -f 1)

	# corroboramos si lo que viene en la variable '$line' esta
	# en la cadena devuelta al invocar el array

	if echo ${stats[@]} | grep -wsq $host; then
		

		continue

	else

		stats[host,$i]=$host	
		stats[cant,$i]=1

	fi

	((i++))


done < $file


echo "
Elementos guardados en el array

${stats[host,0]}
${stats[host,1]}
${stats[host,2]}
${stats[host,3]}
"

