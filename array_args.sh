#!/bin/bash

declare -a arr
i=0
# '$@' devuelve todos los argumentos pasados al script.
# '$#' devuelve la cantidad de argumentos pasados al script.
echo $@
echo $#

# la variable 'arg' asume en cada iteracion el valor de
# cada argumento pasado al script.

for arg in $@; do

	arr[$i]=$arg
	((i++))

done

echo ${arr[0]}
echo ${arr[1]}
echo ${arr[2]}
