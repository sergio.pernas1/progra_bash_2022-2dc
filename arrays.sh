#!/bin/bash


arr=( casa perro arbol bicicleta robot techo )


echo ${arr[0]}
echo ${arr[1]}
echo ${arr[2]}
echo ${arr[3]}
echo ${arr[4]}


echo "
modficar un elemento del array
"

arr[2]=1009

echo ${arr[0]}
echo ${arr[1]}
echo ${arr[2]}
echo ${arr[3]}
echo ${arr[4]}

echo "
mostrar todos los elementos
"

echo ${arr[@]}


echo "
añadir elementos al array
"

arr[10]=9999

echo "${arr[3]} ${arr[5]} ${arr[8]} ${arr[10]}"

echo "
ver los indices ocupados de un array"


echo ${!arr[@]}

echo "
mostrar la cantidad de elementos
"

echo ${#arr[@]}

echo "
mostrar el ultimo elemento del array
"

echo ${arr[-1]}


echo "
mostrar el anteultimo elemento del array
"

echo ${arr[${#arr[@]}-2]} 

elementos=${#arr[@]}

echo ${arr[${elementos}-2]} 


echo "
tamaño de la cadena de un elemento
"

echo ${arr[4]}
echo ${#arr[4]}
